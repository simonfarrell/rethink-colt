# RethinkDB Image for Kubernetes

Copied and updated https://github.com/rosskukulinski/rethinkdb-kubernetes.

Changes:
- Update version of RethinkDB image to ensure apt-transport-https is already installed
- Installed dnsutils to do service lookup
- Edited run.sh to use dig for service lookup
- Added Kubernetes YAML files so everything is in one project
